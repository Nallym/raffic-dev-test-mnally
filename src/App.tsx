import './App.css';
import NavBar from './components/NavBar';
import styled from 'styled-components';
import Form from './components/Form';
import { useLayoutEffect, useState } from 'react';
import { ReactComponent as East } from '@material-design-icons/svg/filled/east.svg';
import { ReactComponent as West } from '@material-design-icons/svg/filled/west.svg';

const Ccc = styled.div`
	background: linear-gradient(
		180deg,
		#b4dfec 0%,
		rgba(192, 225, 236, 0) 65.08%
	);
	background-blend-mode: multiply, normal;
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
	background-color: #eeeeee;
	height: calc(100vh - 70px);
	transition-delay: 1s;
	transition: background 1s ease-in;
	@media only screen and (max-width: 768px) {
		background: linear-gradient(
			180deg,
			#35abd1 0%,
			rgba(53, 171, 209, 0) 65.08%
		);
		background-repeat: no-repeat;
		background-size: auto 85%;
		background-position: left 10% top 0px;
		padding-top: 50px;
	}
	&.img0 {
		background-image: url('./EXT_Burbank_streetscape_v1_FINAL_low-res 1.png');
	}
	&.img1 {
		background-image: url('./tiny-house-condo02-Web.jpg');
	}
	&.img2 {
		background-image: url('./render_t3_03.jpg');
	}
	&.img3 {
		background-image: url('./render_t3_06.jpg');
	}
`;

const SectionOne = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	width: 50%;
	max-width: 729px;
	min-height: 70vh;
	padding-right: 75px;
	padding: 25px 25px 0px 25px;
	@media only screen and (max-width: 768px) {
		width: 100%;
		padding-right: 0px;
	}
`;

const ButtonHolder = styled.div`
	margin-bottom: 40px;

	& button {
		width: 40px;
		height: 40px;
		margin-right: 25px;
		background: #bad982;
		border-radius: 50%;
		border: none;
		fill: white;
		&:hover {
			background: #bae270;
		}
	}
`;

const Heading = styled.h1`
	color: #ffffff;
	font-family: sans-serif;
	font-size: 75px;
	text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.25);
	text-transform: uppercase;
	letter-spacing: -1.09375px;
	@media only screen and (max-width: 768px) {
		font-size: 38px;
	}
`;

const RegisterButton = styled.button`
	color: black;
	padding: 20px 25px;
	background: #bad982;
	font-size: 13px;
	line-height: 16px;
	font-weight: 900;
	letter-spacing: 1.8px;
	text-transform: uppercase;
	border: none;
	outline: none;
	display: none;
	&:hover {
		background: #bae270;
	}
	@media only screen and (max-width: 768px) {
		display: block;
	}
`;

function App() {
	const [backgroundImage, setBackgroundImage] = useState(0);

	const incrementImage = () => {
		setBackgroundImage((backgroundImage + 1) % 4);
	};

	const decreaseImage = () => {
		if (backgroundImage === 0) {
			setBackgroundImage(3);
		} else {
			setBackgroundImage(backgroundImage - 1);
		}
	};

	//autoPlay image
	useLayoutEffect(() => {
		const autoPlay = setTimeout(() => {
			incrementImage();
		}, 4000);
		return () => {
			clearTimeout(autoPlay);
		};
	});

	return (
		<div className='App'>
			<NavBar />
			<Ccc className={'img' + backgroundImage}>
				<SectionOne>
					<div>
						<Heading>
							Two stunning new Townhome Releases Launching <br />
							Early 2021
						</Heading>
						<a href='#RegisterForm'>
							<RegisterButton>Register</RegisterButton>
						</a>
					</div>

					<ButtonHolder>
						<button onClick={decreaseImage}>
							<West />
						</button>
						<button onClick={incrementImage}>
							<East />
						</button>
					</ButtonHolder>
				</SectionOne>
				<Form></Form>
			</Ccc>
		</div>
	);
}

export default App;
