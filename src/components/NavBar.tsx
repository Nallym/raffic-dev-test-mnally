import React, { ReactElement } from 'react';
import logo from '../logo.svg';
import { ReactComponent as PinDrop } from '@material-design-icons/svg/filled/pin_drop.svg';
import { ReactComponent as Call } from '@material-design-icons/svg/filled/call.svg';
import styled from 'styled-components';

const LogoImg = styled.img`
	height: 140%;
`;
const Nav = styled.nav`
	height: 70px;
	display: flex;
	justify-content: center;
	background: linear-gradient(180deg, #bad982 20%, #ffffff 0, #ffffff 50%);
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.05);
	padding: 0px 25px;
`;

const LogoContainer = styled.div`
	display: flex;
	justify-content: space-between;
	width: 1350px;
`;
const ContactContainer = styled.div`
	margin-top: 15px;
	display: flex;
	align-items: center;
	gap: 10px;
`;

const ContactText = styled.a`
	font-size: 12px;
	margin-right: 50px;
	color: black;
	font-weight: bold;
	text-decoration: none;
	@media only screen and (max-width: 768px) {
		display: none;
	}
`;

export default function NavBar(): ReactElement {
	return (
		<Nav>
			<LogoContainer>
				<LogoImg src={logo} alt='logo' height='80px' />
				<ContactContainer>
					<PinDrop href='#' />
					<ContactText href='#'>VISIT OUR SALES CENTRE</ContactText>
					<Call href='tel:555-555-5555' />
					<ContactText href='tel:555-555-5555'>
						1300 354 786
					</ContactText>
				</ContactContainer>
			</LogoContainer>
		</Nav>
	);
}
