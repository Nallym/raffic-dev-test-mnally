import React, { ReactElement, useEffect, useState } from 'react';
import styled from 'styled-components';
import { useForm, ValidationError } from '@formspree/react';

const StyledForm = styled.form`
	max-width: 520px;
	display: flex;
	gap: 5px;
	flex-direction: column;
	background-color: #ffffff;
	padding: 25px;
	margin-top: 100px;
	max-height: 640px;
	@media only screen and (max-width: 768px) {
		width: 100vw;
		background-color: #eeeeee;
		margin-top: 0px;
	}
`;

const ButtonHolder = styled.div`
	margin-top: auto;
	display: flex;
	width: 100%;
	gap: 10px;
`;

const Button = styled.input`
	padding: 15px;
	width: 50%;
	font-size: 16px;
	font-weight: 700;
	border-radius: 4px;
	letter-spacing: 1.8px;
	text-transform: uppercase;
	border: none;
	outline: none;
	&:disabled {
		opacity: 0.5;
	}
	&.green {
		background: #bad982;
		color: black;
	}
	&.blue {
		background: #35abd1;
		color: white;
	}
	&.green:hover {
		background: #bae270;
	}
	&.blue:hover {
		background: #26a2cc;
	}
`;

const TextInput = styled.input`
	color: #292f2d;
	background: linear-gradient(
			0deg,
			rgba(187, 189, 183, 0.1),
			rgba(187, 189, 183, 0.1)
		),
		#ffffff;
	border: 0.5px solid #0b2846;
	box-sizing: border-box;
	border-radius: 4px;
	padding: 15px 15px;
	outline: none;
	font-size: 16px;
	::placeholder,
	::-webkit-input-placeholder {
		color: #292f2d;
	}
	:-ms-input-placeholder {
		color: #292f2d;
	}
`;

const SelectInput = styled.select`
	color: #292f2d;
	background: linear-gradient(
			0deg,
			rgba(187, 189, 183, 0.1),
			rgba(187, 189, 183, 0.1)
		),
		#ffffff;
	border: 0.5px solid #0b2846;
	box-sizing: border-box;
	border-radius: 4px;
	padding: 15px 15px;
	outline: none;
	font-size: 16px;
	background-position-x: 244px;
`;

const StyledCheckBox = styled.label`
	font-size: 13px;

	& input[type='checkbox'] {
		margin: 5px;
		margin-right: 10px;
		transform: scale(1.5);
		padding: 10px;
	}
`;

const ErrorMessage = styled.p`
	color: red;
	font-weight: bold;
`;

function useStickyState(defaultValue: any, key: any) {
	const [value, setValue] = useState(() => {
		const stickyValue = window.localStorage.getItem(key);
		return stickyValue !== null ? JSON.parse(stickyValue) : defaultValue;
	});
	React.useEffect(() => {
		window.localStorage.setItem(key, JSON.stringify(value));
	}, [key, value]);
	return [value, setValue];
}

export default function Form(): ReactElement {
	const [disabled, setDisabled] = useState(false);
	const [errors, setErrors] = useStickyState('', 'eliston-from-errors');
	const [fName, setfName] = useStickyState('', 'eliston-from-fname');
	const [email, setEmail] = useStickyState('', 'eliston-from-email');
	const [phone, setPhone] = useStickyState('', 'eliston-from--phone');
	const [select, setSelect] = useStickyState('1', 'eliston-from-select');
	const [check, setCheck] = useStickyState(
		{
			1: false,
			2: false,
			3: false,
			4: false,
			5: false,
			6: false,
			7: false,
			8: false,
		},
		'eliston-from-check'
	);

	useEffect(() => {
		setErrors('');
		let count = 0;
		for (var p in check) {
			if (check.hasOwnProperty(p) && check[p] === true) {
				count++;
			}
		}
		if (count <= 1) {
			setDisabled(true);
			setErrors('Select 2 or more services');
		}
		if (count >= 6) {
			setDisabled(true);
			setErrors('Select 5 or fewer services');
		}
	}, [check, setErrors]);

	const [state, handleSubmit] = useForm('xwkaweee');
	if (state.succeeded) {
		return <p>Thanks for joining!</p>;
	}

	const handleCheckChange = (e: any) => {
		const { name, checked } = e.target;
		setCheck((prevState: any) => ({
			...prevState,
			[name]: checked,
		}));
	};

	const handelReset = () => {
		setCheck({
			1: false,
			2: false,
			3: false,
			4: false,
			5: false,
			6: false,
			7: false,
			8: false,
		});
		setEmail('');
		setPhone('');
		setfName('');
		setSelect(1);
	};

	return (
		<StyledForm onSubmit={handleSubmit} id='RegisterForm'>
			<h3>
				Be the first to register for new townhome releases for first
				option
			</h3>
			<TextInput
				type='text'
				name='firstName'
				id='firstName'
				placeholder='First Name*'
				value={fName}
				onChange={(e) => setfName(e.target.value)}
				required
			/>
			<ValidationError
				prefix='FirstName'
				field='firstName'
				errors={state.errors}
			/>
			<TextInput
				type='emial'
				name='email'
				id='emailinput'
				placeholder='Email*'
				value={email}
				onChange={(e) => setEmail(e.target.value)}
				required
			/>
			<ValidationError
				prefix='Email'
				field='email'
				errors={state.errors}
			/>
			<TextInput
				type='phone'
				name='Phone'
				id='phoneinput'
				placeholder='Phone*'
				value={phone}
				onChange={(e) => setPhone(e.target.value)}
				required
			/>
			<ValidationError
				prefix='Phone'
				field='phone'
				errors={state.errors}
			/>
			<SelectInput
				name='lookingtobuy'
				id='buyselection'
				defaultValue={select}
				onChange={(e) => setSelect(e.target.value)}
			>
				<option value='1' disabled hidden>
					When are you looking to buy
				</option>
				<option value='now'>Now</option>
				<option value='tomorrow'>Tomorrow</option>
				<option value='nextWeek'>Next Week</option>
				<option value='never'>Never</option>
			</SelectInput>

			<ValidationError
				prefix='Selector'
				field='buyselection'
				errors={state.errors}
			/>
			<StyledCheckBox htmlFor='check1'>
				<input
					type='checkbox'
					name='1'
					id='check1'
					checked={check[1]}
					onChange={(e) => handleCheckChange(e)}
				/>
				Value 1
			</StyledCheckBox>
			<StyledCheckBox htmlFor='check2'>
				<input
					type='checkbox'
					name='2'
					id='check2'
					checked={check[2]}
					onChange={(e) => handleCheckChange(e)}
				/>
				Value 2
			</StyledCheckBox>
			<StyledCheckBox htmlFor='check3'>
				<input
					type='checkbox'
					name='3'
					id='check3'
					checked={check[3]}
					onChange={(e) => handleCheckChange(e)}
				/>
				Value 3
			</StyledCheckBox>
			<StyledCheckBox htmlFor='check4'>
				<input
					type='checkbox'
					name='4'
					id='check4'
					checked={check[4]}
					onChange={(e) => handleCheckChange(e)}
				/>
				Value 4
			</StyledCheckBox>
			<StyledCheckBox htmlFor='check5'>
				<input
					type='checkbox'
					name='5'
					id='check5'
					checked={check[5]}
					onChange={(e) => handleCheckChange(e)}
				/>
				Value 5
			</StyledCheckBox>
			<StyledCheckBox htmlFor='check6'>
				<input
					type='checkbox'
					name='6'
					id='check6'
					checked={check[6]}
					onChange={(e) => handleCheckChange(e)}
				/>
				Value 6
			</StyledCheckBox>
			<StyledCheckBox htmlFor='check7'>
				<input
					type='checkbox'
					name='7'
					id='check7'
					checked={check[7]}
					onChange={(e) => handleCheckChange(e)}
				/>
				Value 7
			</StyledCheckBox>
			<StyledCheckBox htmlFor='check8'>
				<input
					type='checkbox'
					name='8'
					id='check8'
					checked={check[8]}
					onChange={(e) => handleCheckChange(e)}
				/>
				Value 8
			</StyledCheckBox>
			<ValidationError
				prefix='Checkbox'
				field='check8'
				errors={state.errors}
			/>
			<ErrorMessage>{errors}</ErrorMessage>
			<ButtonHolder>
				<Button
					className='green'
					type='submit'
					value='submit'
					disabled={state.submitting || disabled}
				></Button>
				<Button
					className='blue'
					type='button'
					value='reset'
					onClick={handelReset}
					disabled={state.submitting}
				></Button>
			</ButtonHolder>
		</StyledForm>
	);
}
